import click


@click.group()
def cli():
    pass


@cli.command()
def hello():
    click.echo('hello')


@cli.command()
def world():
    click.echo('world')


if __name__ == '__main__':
    cli()
