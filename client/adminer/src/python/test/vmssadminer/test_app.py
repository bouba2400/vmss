from click.testing import CliRunner
from vmssadminer.app import hello, world


class TestApp:
    def test_hello(self):
        runner = CliRunner()
        result = runner.invoke(hello)
        assert result.exit_code == 0
        assert result.output == 'hello\n'

    def test_world(self):
        runner = CliRunner()
        result = runner.invoke(world)
        assert result.exit_code == 0
        assert result.output == 'world\n'
