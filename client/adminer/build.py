import os
import sys


def run(command):
    code = os.system(command)
    if code != 0:
        sys.exit(code)


if __name__ == "__main__":
    run("pipenv install")
    run("coverage run -m pytest src/python/test/")
    run("coverage report --omit *test* --fail-under 10")
    run("pylama src/python")
    run("pipenv-setup sync")
    run("setup.py sdist -d build/dist bdist_wheel")
