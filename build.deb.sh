#!/usr/bin/env bash

cd client/adminer
python3 build.py
cd -

mkdir -p build/dist
mkdir -p build/dist/bin
mkdir -p build/dist/lib/python/wheels

cp client/adminer/build/dist/* build/dist/lib/python/wheels/
cp setup/bin/* build/dist/bin/
