FROM alpine:3.7
RUN apk add python3 py3-pip
COPY build/dist/* /opt/vmss/
WORKDIR /opt/vmss/
RUN sh bin/install.sh
ENTRYPOINT ["vmssadminer"]
CMD ["--help"]
