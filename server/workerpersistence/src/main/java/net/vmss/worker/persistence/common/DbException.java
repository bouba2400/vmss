package net.vmss.worker.persistence.common;

public class DbException extends Exception {
  public DbException(final Throwable throwable) {
    super(throwable);
  }
}
