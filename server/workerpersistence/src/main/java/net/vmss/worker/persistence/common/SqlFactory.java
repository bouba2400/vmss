package net.vmss.worker.persistence.common;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class SqlFactory {
  public Connection makeConnection(final String url) throws SQLException {
    return DriverManager.getConnection(url);
  }
}
