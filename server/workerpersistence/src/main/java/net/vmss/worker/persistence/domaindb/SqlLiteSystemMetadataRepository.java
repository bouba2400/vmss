package net.vmss.worker.persistence.domaindb;

import net.vmss.worker.domain.model.SystemMetadata;
import net.vmss.worker.domain.model.SystemSettings;
import net.vmss.worker.domain.model.SystemStatus;
import net.vmss.worker.domain.service.SystemMetadataRepository;
import org.apache.commons.lang3.NotImplementedException;

public class SqlLiteSystemMetadataRepository implements SystemMetadataRepository {

  @Override
  public SystemMetadata find() {
    return new SystemMetadata(new SystemSettings(), SystemStatus.PENDING);
  }

  @Override
  public void save(final SystemMetadata systemMetadata) {
    throw new NotImplementedException();
  }
}
