package net.vmss.worker.core;

import net.vmss.worker.domain.service.SystemMetadataRepository;
import net.vmss.worker.persistence.domaindb.DomainDbFacade;
import net.vmss.worker.persistence.domaindb.SqlLiteSystemMetadataRepository;

public class DefaultServiceContext implements ServiceContext {
  @Override
  public SystemInitializer getSystemInitializer() {
    return new DefaultSystemInitializer(this);
  }

  DomainDbFacade getDomainDbFacade() {
    return new DomainDbFacade();
  }

  @Override
  public SystemMetadataRepository getSystemMetadataRepository() {
    return new SqlLiteSystemMetadataRepository();
  }
}
