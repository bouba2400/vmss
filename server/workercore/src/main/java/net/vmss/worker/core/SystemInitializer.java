package net.vmss.worker.core;

public interface SystemInitializer {
  void initializeSystem();
}
