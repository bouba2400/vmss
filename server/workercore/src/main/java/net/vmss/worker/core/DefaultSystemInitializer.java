package net.vmss.worker.core;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

@Getter
@AllArgsConstructor
@Slf4j
public class DefaultSystemInitializer implements SystemInitializer {
  private DefaultServiceContext serviceContext;

  @Override
  public void initializeSystem() {
    serviceContext.getDomainDbFacade().initializeDb();
    loadSystemDefinitionsAsync();
  }

  private void loadSystemDefinitionsAsync() {
    log.info("tbi");
  }
}
