package net.vmss.worker.core;

import net.vmss.worker.domain.service.SystemMetadataRepository;

public interface ServiceContext {
  SystemInitializer getSystemInitializer();

  SystemMetadataRepository getSystemMetadataRepository();
}
