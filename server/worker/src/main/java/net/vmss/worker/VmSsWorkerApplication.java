package net.vmss.worker;

import net.vmss.worker.core.DefaultServiceContext;
import net.vmss.worker.core.ServiceContext;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.event.ApplicationStartedEvent;
import org.springframework.context.annotation.Bean;
import org.springframework.context.event.EventListener;

@SpringBootApplication
public class VmSsWorkerApplication {
  public static void main(final String[] args) {
    try {
      final SpringApplication springApplication = new SpringApplication(VmSsWorkerApplication.class);
      springApplication.run(args);
    } catch (final Exception e) {
      e.printStackTrace();
    }
  }

  @Bean
  public ServiceContext serviceContext() {
    return new DefaultServiceContext();
  }

  @EventListener
  public void onApplicationStarted(final ApplicationStartedEvent event) {
    serviceContext().getSystemInitializer().initializeSystem();
  }
}
