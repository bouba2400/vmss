package net.vmss.worker.domain.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.apache.commons.lang3.NotImplementedException;

import java.util.Set;

@Getter
@AllArgsConstructor
public class Transaction {
  private Long identifier;
  private TransactionInfo info;
  private Integer shelfNumber;
  private TransactionStatus status;
  private Set<CoinPayment> payments;
  private Set<CoinRefund> refunds;

  public void addPayment(final CoinPayment payment) {
    throw new NotImplementedException();
  }

  public void addRefund(final CoinRefund refund) {
    throw new NotImplementedException();
  }

  public void initiate() {
    throw new NotImplementedException();
  }

  public void complete(final ItemShelf shelf) {
    throw new NotImplementedException();
  }

  public void cancel() {
    throw new NotImplementedException();
  }

  public void expire() {
    throw new NotImplementedException();
  }
}
