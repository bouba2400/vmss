package net.vmss.worker.domain.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class CoinRefund {
  private Long identifier;
  private CoinRefundInfo info;
}
