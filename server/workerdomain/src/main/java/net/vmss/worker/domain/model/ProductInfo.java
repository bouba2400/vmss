package net.vmss.worker.domain.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ProductInfo {
  private String name;
  private Integer price;
}
