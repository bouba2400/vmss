package net.vmss.worker.domain.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class Product {
  private String code;
  private ProductInfo info;
}
