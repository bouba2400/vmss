package net.vmss.worker.domain.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class TransactionInfo {
  private String productCode;
  private String productName;
  private Integer productPrice;
}
