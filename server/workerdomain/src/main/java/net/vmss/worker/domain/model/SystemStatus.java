package net.vmss.worker.domain.model;

public enum SystemStatus {
  PENDING,
  UP,
  DOWN,
  FAULT
}
