package net.vmss.worker.domain.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class CoinPayment {
  private Long identifier;
  private CoinPaymentInfo info;
}
