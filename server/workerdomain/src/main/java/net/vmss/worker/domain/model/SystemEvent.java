package net.vmss.worker.domain.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class SystemEvent {
  private Long identifier;
  private SystemEventInfo info;
}
