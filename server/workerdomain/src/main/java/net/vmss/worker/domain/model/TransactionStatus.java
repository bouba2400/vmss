package net.vmss.worker.domain.model;

public enum TransactionStatus {
  PENDING,
  INITIATED,
  CANCELLED,
  EXPIRED,
  COMPLETE
}
