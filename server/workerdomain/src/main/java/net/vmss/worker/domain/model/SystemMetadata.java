package net.vmss.worker.domain.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.apache.commons.lang3.NotImplementedException;

@Getter
@AllArgsConstructor
public class SystemMetadata {
  private SystemSettings settings;

  private SystemStatus status;

  public void markUp() {
    status = SystemStatus.UP;
  }

  public void markDown() {
    throw new NotImplementedException();
  }

  public void changeSettings(final SystemSettings newSettings) {
    throw new NotImplementedException();
  }
}
