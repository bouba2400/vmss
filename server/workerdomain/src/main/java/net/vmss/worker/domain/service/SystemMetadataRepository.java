package net.vmss.worker.domain.service;

import net.vmss.worker.domain.model.SystemMetadata;

public interface SystemMetadataRepository {
  SystemMetadata find();

  void save(SystemMetadata systemMetadata);
}
