package net.vmss.worker.domain.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.apache.commons.lang3.NotImplementedException;

@Getter
@AllArgsConstructor
public class ItemShelf {
  private Integer number;
  private ItemShelfInfo info;
  private String productCode;

  public void decrementStock() {
    throw new NotImplementedException();
  }
}
