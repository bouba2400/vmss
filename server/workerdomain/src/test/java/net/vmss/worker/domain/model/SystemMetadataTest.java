package net.vmss.worker.domain.model;

import lombok.NoArgsConstructor;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

@NoArgsConstructor
public class SystemMetadataTest {

  @Test
  public void bringUp() {
    final SystemMetadata systemMetadata = new SystemMetadata(new SystemSettings(), SystemStatus.PENDING);
    systemMetadata.markUp();
    Assertions.assertEquals(SystemStatus.UP, systemMetadata.getStatus());
  }

  @Test
  public void bringDown() {
  }

  @Test
  public void changeInfo() {
  }
}
